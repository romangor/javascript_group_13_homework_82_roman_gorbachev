import { createAction, props } from '@ngrx/store';


export const setSings = createAction('[Calculator] Sum', props<{value: string}>());
export const setNumbers = createAction('[Calculator] setNumbers', props<{value: string}>());
