import { createReducer, on } from '@ngrx/store';
import { setNumbers, setSings } from './calculator.actions';

const initialState: string = '';

export const calculatorReducer = createReducer(
  initialState,
  on(setSings, (state, {value}) => {
   try {
     let result = state;
     let lastSymbol = state.substr(-1, 1);
     if(value === '='){
       return eval(result).toString();
     }
     if(value === lastSymbol){
       return state;
     }
     return result += value;
   } catch (e) {
     alert('Enter correct expression');
   }
  }),

  on(setNumbers, (state, {value}) => {
    let result = state;
    if(value === 'AC'){
      return result = '';
    }
    if(value === 'C'){
      return result.substring(0, result.length -1);
    }
    return result += value;
  })
);
