import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { setNumbers, setSings, } from './calculator.actions';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.sass']
})
export class CalculatorComponent {
  calc!: Observable<string>;

  numbers = ['1','2','3','4','5','6','7','8','9','0', 'AC', 'C'];
  symbols: string[] = ['*', '/', '-', '+', '='];

  constructor(private store: Store<{calc: string}>) {
    this.calc = store.select('calc');
  }

  setOfNumbers(index: string){
    this.store.dispatch(setNumbers({value: index}))
  }

  setOfSing(symbol: string) {
    this.store.dispatch(setSings({value: symbol}))
  }
}
